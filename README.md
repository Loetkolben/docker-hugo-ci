# docker-hugo-ci

A docker image primarily aimed at CI servers for building
[hugo](https://gohugo.io/) websites.

Currently assembled by just installing the latest `hugo` binary into an
`alpine:latest` image.


## Tag Policy
- `latest` and `ref-master` are synonymous. They point to the latest image
  built by the `master` branch. This image is automatically rebuilt every week,
  the `hugo-` tags will be updated appropriately.
- `hugo-$HUGO_VER` points to the latest `master` image that has this version.
- `ref-$BRANCH_NAME` points to the latest image built from the branch.
- `commit-$COMMIT_SHA` points to the latest image built from the commit.
- `build-$COMMIT_SHA-$DATE` is the result of the specified commit at the
  specified time.
- All tags except `latest` automatically expire after 90 days.
- We keep at least 25 tags around. 
