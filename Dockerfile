FROM alpine:latest

# OpenContainers Label Specification:
# https://github.com/opencontainers/image-spec/blob/main/annotations.md
LABEL org.opencontainers.image.title="Hugo CI Container"
LABEL org.opencontainers.image.description="Docker Image for CI Servers featuring hugo (a static site generator), based on alpine:latest"
# authors, urls and so forth are probably better set at build time

# Hugo version, either a version number or LATEST
ARG VERSION="LATEST"

RUN set -eu; \
    HUGO_VER=${VERSION:-LATEST}; \
    if [ "$HUGO_VER" = "LATEST" ]; then \
        latest_url=$(wget -S --spider -q https://github.com/gohugoio/hugo/releases/latest 2>&1 | grep -i "Location:"); \
        HUGO_VER=${latest_url##*/v}; \
        echo "Latest hugo release is '$HUGO_VER'"; \
    fi; \
    ARCH=${ARCH:-UNKOWN}; \
    if [ "$ARCH" = "UNKOWN" ]; then \
        case $(uname -m) in \
            "x86_64") \
                ARCH="64bit" \
                ;; \
            "armv7l") \
                ARCH="ARM" \
                ;; \
            *) \
                echo "FATAL: Architecure autodection failed. I do not know what 'uname -m' result '$(uname -m)' means." \
                exit 1 \
                ;; \
        esac \
    fi; \
    HUGO_DL_URL="https://github.com/gohugoio/hugo/releases/download/v${HUGO_VER}/hugo_${HUGO_VER}_Linux-${ARCH}.tar.gz" \
    HUGO_DEST="/usr/local/bin/hugo"; \
    wget -q -O - "$HUGO_DL_URL" | tar -zx hugo -O > "$HUGO_DEST"; \
    chmod +x "$HUGO_DEST"
